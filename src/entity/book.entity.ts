import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Author } from "./author.entity";

@Entity()
export class Book {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false, length: 64})
    title: string;

    @Column({nullable: false, length: 1024})
    description: string;

    @ManyToOne(() => Author, author => author.books, {nullable: false})
    author: Author;

    @Column({nullable: false, length: 128})
    imageURL: string;

    @Column({nullable: false, default: 0})
    rating: number;
}