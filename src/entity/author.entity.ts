import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Book } from "./book.entity";

@Entity()
export class Author {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false, length: 64})
    firstName: string;

    @Column({nullable: false, length: 64})
    lastName: string;

    @OneToMany(() => Book, book => book.author)
    books?: Book[];
}