export class AuthorDTO {
    id: number;
    firstName: string;
    lastName: string;
}