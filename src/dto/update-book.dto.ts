import { AuthorDTO } from "./author.dto";

export class UpdateBookDTO {
    title?: string;
    description?: string;
    author?: AuthorDTO;
    rating?: number;
    imageURL?: string;
}