import { AuthorDTO } from "./author.dto";

export class CreateBookDTO {
    title: string;
    description: string;
    author: AuthorDTO;
    imageURL: string;
}