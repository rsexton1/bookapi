import {MigrationInterface, QueryRunner} from "typeorm";

export class AddImageToBook1635932416224 implements MigrationInterface {
    name = 'AddImageToBook1635932416224'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`imageURL\` varchar(128) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`imageURL\``);
    }

}
