import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateBook1635770989520 implements MigrationInterface {
    name = 'CreateBook1635770989520'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`book\` (\`id\` int NOT NULL AUTO_INCREMENT, \`title\` varchar(64) NOT NULL, \`description\` varchar(1024) NOT NULL, \`author\` varchar(64) NOT NULL, \`rating\` int NOT NULL DEFAULT '0', PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`book\``);
    }

}
