import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateAuthorUpdateBook1635777939234 implements MigrationInterface {
    name = 'CreateAuthorUpdateBook1635777939234'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` CHANGE \`author\` \`authorId\` varchar(64) NOT NULL`);
        await queryRunner.query(`CREATE TABLE \`author\` (\`id\` int NOT NULL AUTO_INCREMENT, \`firstName\` varchar(64) NOT NULL, \`lastName\` varchar(64) NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`authorId\``);
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`authorId\` int NULL`);
        await queryRunner.query(`ALTER TABLE \`book\` ADD CONSTRAINT \`FK_66a4f0f47943a0d99c16ecf90b2\` FOREIGN KEY (\`authorId\`) REFERENCES \`author\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`book\` DROP FOREIGN KEY \`FK_66a4f0f47943a0d99c16ecf90b2\``);
        await queryRunner.query(`ALTER TABLE \`book\` DROP COLUMN \`authorId\``);
        await queryRunner.query(`ALTER TABLE \`book\` ADD \`authorId\` varchar(64) NOT NULL`);
        await queryRunner.query(`DROP TABLE \`author\``);
        await queryRunner.query(`ALTER TABLE \`book\` CHANGE \`authorId\` \`author\` varchar(64) NOT NULL`);
    }

}
