import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { CreateAuthorDTO } from 'src/dto/create-author.dto';
import { CreateBookDTO } from 'src/dto/create-book.dto';
import { UpdateAuthorDTO } from 'src/dto/update-author.dto';
import { UpdateBookDTO } from 'src/dto/update-book.dto';
import { BookService } from './book.service';

@Controller()
export class BookController {
    constructor(private readonly bookService: BookService) {}

    @Get('books')
    public async getAllBooks() {
        const res = await this.bookService.getAllBooks();
        return res;
    }

    @Get('books/:bookID')
    public async getOneBook(@Param('bookID') bookID: number) {
        const res = await this.bookService.getOneBook(bookID);
        return res;
    }

    @Post('books')
    public async createBook(@Body() createBookRequest: CreateBookDTO) {
        const res = await this.bookService.createBook(createBookRequest);
        return res;
    }

    @Put('books/:bookID')
    public async updateBook(@Param('bookID') bookID: number, @Body() updateBookRequest: UpdateBookDTO) {
        const res = await this.bookService.updateBook(bookID, updateBookRequest);
        return res;
    }

    @Delete('books/:bookID')
    @HttpCode(HttpStatus.NO_CONTENT)
    public async deleteBook(@Param('bookID') bookID: number) {
        await this.bookService.deleteBook(bookID);
    }



    @Get('authors')
    public async getAllAuthors() {
        const res = await this.bookService.getAllAuthors();
        return res;
    }

    @Get('authors/:authorID')
    public async getOneAuthor(@Param('authorID') authorID: number) {
        const res = await this.bookService.getOneAuthor(authorID);
        return res;
    }

    @Post('authors')
    public async createAuthor(@Body() createAuthorRequest: CreateAuthorDTO) {
        const res = await this.bookService.createAuthor(createAuthorRequest);
        return res;
    }

    @Put('authors/:authorID')
    public async updateAuthor(@Param('authorID') authorID: number, @Body() updateAuthorRequest: UpdateAuthorDTO) {
        const res = await this.bookService.updateAuthor(authorID, updateAuthorRequest);
        return res;
    }

    @Delete('authors/:authorID')
    @HttpCode(HttpStatus.NO_CONTENT)
    public async deleteAuthor(@Param('authorID') authorID: number) {
        await this.bookService.deleteAuthor(authorID);
    }
}
