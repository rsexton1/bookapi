import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthorDTO } from 'src/dto/author.dto';
import { BookDTO } from 'src/dto/book.dto';
import { CreateAuthorDTO } from 'src/dto/create-author.dto';
import { CreateBookDTO } from 'src/dto/create-book.dto';
import { UpdateAuthorDTO } from 'src/dto/update-author.dto';
import { UpdateBookDTO } from 'src/dto/update-book.dto';
import { Author } from 'src/entity/author.entity';
import { Book } from 'src/entity/book.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BookService {
    constructor(@InjectRepository(Book) private bookRepository: Repository<Book>, @InjectRepository(Author) private authorRepository: Repository<Author>) {}

    private bookEntityToDTO(book: Book): BookDTO {
        const bookDTO = new BookDTO();
        bookDTO.id = book.id;
        bookDTO.title = book.title;
        bookDTO.description = book.description;
        bookDTO.author = book.author;
        bookDTO.imageURL = book.imageURL;
        bookDTO.rating = book.rating;

        return bookDTO;
    }
    private authorEntityToDTO(author: Author): AuthorDTO {
        const authorDTO = new AuthorDTO();
        authorDTO.id = author.id;
        authorDTO.firstName = author.firstName;
        authorDTO.lastName = author.lastName;

        return authorDTO;
    }



    public async getAllBooks() {
        const books: Book[] = await this.bookRepository.find({relations: ["author"]});
        const booksDTO: BookDTO[] = books.map(book => this.bookEntityToDTO(book))
        return booksDTO;
    }

    public async getOneBook(id: number) {
        const book: Book = await this.bookRepository.findOne(id, {relations: ["author"]});
        if(!book) throw new NotFoundException(`Book with the ID: ${id} was not found`);

        const bookDTO: BookDTO = this.bookEntityToDTO(book);
        return bookDTO;
    }

    public async createBook(createBookRequest: CreateBookDTO) {
        const book: Book = new Book();
        book.title = createBookRequest.title;
        book.description = createBookRequest.description;
        book.author = createBookRequest.author;
        book.imageURL = createBookRequest.imageURL;

        await this.bookRepository.save(book);

        const bookDTO: BookDTO = await this.getOneBook(book.id);
        return bookDTO;
    }

    public async updateBook(id: number, updateBookRequest: UpdateBookDTO) {
        const book: Book = await this.getOneBook(id);

        if(updateBookRequest.title) {book.title = updateBookRequest.title}
        if(updateBookRequest.description) {book.description = updateBookRequest.description}
        if(updateBookRequest.author) {book.author = updateBookRequest.author}
        if(updateBookRequest.imageURL) {book.imageURL = updateBookRequest.imageURL}
        if(updateBookRequest.rating || updateBookRequest.rating == 0) {
            if(updateBookRequest.rating >= 0 && updateBookRequest.rating <= 5) {
                book.rating = updateBookRequest.rating
            } else {
                throw new BadRequestException('Rating must be between 0 and 5');
            }
            
        }

        await this.bookRepository.save(book);

        const bookDTO: BookDTO = await this.getOneBook(book.id);
        return bookDTO;
    }

    public async deleteBook(id: number) {
        const book: Book = await this.getOneBook(id);
        await this.bookRepository.remove(book);
    }



    public async getAllAuthors() {
        const authors: Author[] = await this.authorRepository.find();
        const authorsDTO: AuthorDTO[] = authors.map(author => this.authorEntityToDTO(author));
        return authorsDTO;
    }

    public async getOneAuthor(id: number) {
        const author: Author = await this.authorRepository.findOne(id);
        if(!author) throw new NotFoundException(`Author with the ID: ${id} was not found`);
        const authorDTO: AuthorDTO = this.authorEntityToDTO(author);
        return authorDTO;
    }

    public async createAuthor(createAuthorRequest: CreateAuthorDTO) {
        const author: Author = new Author();
        author.firstName = createAuthorRequest.firstName;
        author.lastName = createAuthorRequest.lastName;

        await this.authorRepository.save(author);

        const authorDTO: AuthorDTO = this.authorEntityToDTO(author);
        return authorDTO;
    }

    public async updateAuthor(id: number, updateAuthorRequest: UpdateAuthorDTO) {
        const author: Author = await this.getOneAuthor(id);

        if(updateAuthorRequest.firstName) {author.firstName = updateAuthorRequest.firstName}
        if(updateAuthorRequest.lastName) {author.lastName = updateAuthorRequest.lastName}

        await this.authorRepository.save(author);

        const authorDTO = this.authorEntityToDTO(author);
        return authorDTO;
    }

    public async deleteAuthor(id: number) {
        const searchAuthor: Author = await this.getOneAuthor(id);
        const books: Book[] = await this.bookRepository.find({ where: {author: searchAuthor}});
        if(books.length === 0) {
            await this.authorRepository.remove(searchAuthor);
        } else {
            throw new BadRequestException(`Author with the ID: ${id} is currently used in books table - you must remove dependant books before deleting author`);
        }
    }
}
