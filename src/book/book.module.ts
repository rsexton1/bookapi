import { Module } from '@nestjs/common';
import { BookService } from './book.service';
import { BookController } from './book.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book } from 'src/entity/book.entity';
import { Author } from 'src/entity/author.entity';

@Module({
  providers: [BookService],
  controllers: [BookController],
  imports: [TypeOrmModule.forFeature([Book]), TypeOrmModule.forFeature([Author])]
})
export class BookModule {}
